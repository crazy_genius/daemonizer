package command

import (
	"log"
	"os"
	"os/exec"
	"syscall"

	"github.com/urfave/cli/v2"
)

var daemonizeCommand = &cli.Command{
	Name:    "daemonize",
	Aliases: []string{"d"},
	Usage:   "damonizer d {path to binnary}",
	Action:  daemonizeAction,
}

func daemonizeAction(c *cli.Context) error {
	contextArgs := c.Args()

	if contextArgs.Len() < 1 {
		log.Fatal("no binnary to daemonize provided")
	}

	binnaryPath := contextArgs.Get(0)

	binary, lookErr := exec.LookPath(binnaryPath)
	if lookErr != nil {
		panic(lookErr)
	}

	args := []string{binnaryPath}
	env := os.Environ()

	pid, execErr := syscall.ForkExec(binary, args, &syscall.ProcAttr{
		Env: env,
	})
	if execErr != nil {
		panic(execErr)
	}

	log.Println("Process forked new proc pid is ", pid)

	return nil
}
