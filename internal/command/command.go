package command

import "github.com/urfave/cli/v2"

// RegisteredCommands containes all registered cli commands
var RegisteredCommands = []*cli.Command{
	daemonizeCommand,
}
