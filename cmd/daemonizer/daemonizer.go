package main

import (
	"log"
	"os"

	"bitbucket.org/crazy_genius/daemonizer/internal/command"
	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name:     "daemonizer",
		Usage:    "run an binnary a daemon",
		Commands: command.RegisteredCommands,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
